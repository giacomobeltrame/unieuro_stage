var path = require('path');
var express = require('express');
var logger = require('morgan');
var app = express();

// Log the requests
app.use(logger('dev'));

// Serve static files
app.use(express.static(__dirname));  // statics resources for all /Quizzipedia path

// Route for everything else.
app.get('*', function(req, res){
  res.sendFile(path.resolve('./index.html'));
});

// Fire it up!
app.listen(3000);
console.log('Listening on port 3000');