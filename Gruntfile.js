module.exports = function(grunt) {

    var libs_dir = 'scripts/libs/';
    var libs_min_dir = 'scripts/libs_min/';
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        'jshint': {
            default: ['Gruntfile.js','scripts/main/main.js',libs_dir+'tabber/tabber.js',libs_dir+'menu_toggler/menu_toggler.js',libs_dir+'displayers/displayers.js',libs_dir+'injecter/injecter.js',libs_dir+'menu_icon_animator/menu_icon_animator.js']
        },

        'requirejs': {
            compile: {
                options: {
                    baseUrl: "./bower_components",
                    include: [ '../scripts/main/main' ],
                    out: "scripts/main/main_min.js",
                    findNestedDepencencies: true,
                    //wrap: true,
                    //wrapShim: true

                    paths: {
                        'injecter': '../scripts/libs/injecter/injecter',
                        'menu_icon_animator': '../scripts/libs/menu_icon_animator/menu_icon_animator',
                        'requirejs': 'requirejs/requirejs',
                        'jquery': 'jquery/dist/jquery',
                        'slick-carousel': 'slick-carousel/slick/slick',
                        'slide-and-swipe-menu': 'slide-and-swipe-menu/jquery.slideandswipe',
                        'jquery-touchswipe': 'jquery-touchswipe/jquery.touchSwipe.min',
                        'mustache': 'mustache.js/mustache.min',
                        'sticky-kit': 'sticky-kit/jquery.sticky-kit',
                        'ScrollIt': 'ScrollIt/scrollIt',
                        'fancybox': 'fancybox/source/jquery.fancybox',
                        'fancybox-thumbs': 'fancybox/source/helpers/jquery.fancybox-thumbs'
                    },

                    optimize: 'uglify2',

                    uglify2: {
                        output: {
                            beautify: false
                        },
                        compress: {
                            sequences: false,
                            global_defs: {
                                DEBUG: false
                            }
                        },
                        warnings: true,
                        mangle: false,
                    },

                },

                shim: {
                    'slide-and-swipe-menu': {
                        deps: ['menu_icon_animator'],
                        exports: 'slide-and-swipe-menu'
                    }
                }
                
            }
        },

        'sass': {
            default: {
                files: {
                    'stylesheets/main.css': 'stylesheets/main.scss'
                }
            }
        },

        'cssmin': {
            default: {
                files: {
                    'stylesheets/main_min.css': 'stylesheets/main.css'
                }
            }
        },

        'clean': {
            build: {
                src: [ 'stylesheets/main.css.map' ]
            }
        },

        'watch': {
          
            stylesheets: { 
                files: ['stylesheets/**/*.scss'], 
                tasks: ['sass', 'cssmin', 'clean']
            },

            scripts: { 
                files: ['Gruntfile.js', 'config.js', 'scripts/main/main.js', libs_dir+'tabber/tabber.js', libs_dir+'menu_toggler/menu_toggler.js', libs_dir+'displayers/displayers.js', libs_dir+'injecter/injecter.js', libs_dir+'menu_icon_animator/menu_icon_animator.js'],
                tasks: ['jshint', 'requirejs'] 
            } 
        }

    });

    grunt.registerTask('foo', 'A sample task.', function(arg1, arg2) {
        if (arguments.length === 0) {
            grunt.log.writeln(this.name + ", no args");
        } else {
            grunt.log.writeln(this.name + ", " + arg1 + " " + arg2);
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');     //js syntax controller
    grunt.loadNpmTasks('grunt-contrib-uglify');     //js minifier
    grunt.loadNpmTasks('grunt-contrib-watch');      //file-change watcher
    grunt.loadNpmTasks('grunt-contrib-sass');       //css generator
    grunt.loadNpmTasks('grunt-contrib-cssmin');     //css minifier
    grunt.loadNpmTasks('grunt-contrib-requirejs');  //used to uglify js
    grunt.loadNpmTasks('grunt-contrib-clean');      //used to uglify js

    grunt.registerTask('default',['jshint','requirejs','sass','cssmin']);

};
