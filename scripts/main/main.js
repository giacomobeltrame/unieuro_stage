require(['menu_icon_animator','injecter','jquery','slick-carousel','sticky-kit','ScrollIt','fancybox','fancybox-thumbs'],
	function(menu_icon_animator,injecter) {

		var $level2 = $('.level2');

		var $cart_counter_container = $('#cart_counter_container'),
			$line1 = $('#line1'),
			$line2 = $('#line2'),
			$line3 = $('#line3');
			$panel_content = $('#panel_content');

		// navbar ----------------------------------------------------------
		$.scrollIt({
			topOffset: -130,
			upKey: 0,             // key code to navigate to the next section
			downKey: 0 
		});

		// fancybox ----------------------------------------------------------------------
		$(".fancybox").fancybox({
			prevEffect	: 'elastic',
			nextEffect	: 'elastic',
			//scrolling: 'yes',
			helpers	: {
				title	: {
					type: 'outside'
				},
				thumbs	: {
					width	: 50,
					height	: 50
				},
				overlay : {
		            css : {
		                'background' : 'rgba(0, 0, 0, 0.8)'
		            },
		            locked : true
		      	}
			}
		});

		// sticky-kit-------------------------------------------------
		$('.navbar').stick_in_parent({
			offset_top: 80,
		}).on('sticky_kit:stick', function(e) {
			//$('.navbar').removeAttr('style');
		});

		//left/right panel --------------------------------------------------
		$('.toggle_menu').on('click',function(){
			toggle_menu();
			$('body').css({
				'overflow-x': 'hidden'
			});
		});
		$('#cart_icon').on('click',function(){
			toggle_cart();
		});

		$('.cart_title').on('click','img',function(){
			toggle_cart();
		});

		$('.dark_layer').on('click',function(){
			if($('.wrapper').hasClass('wrapper_active_right'))
				toggle_menu();
			else
				toggle_cart();
		});

		function toggle_menu(){
			$('.wrapper').toggleClass('wrapper_active_right wrapper_unactive');
			$('#left_panel').toggleClass('left_panel_active left_panel_unactive');
			$('.dark_layer').toggleClass('layer_active layer_unactive');
			$('body').toggleClass('scrollable not_scrollable');
			$('.dark_layer').toggleClass('wrapper_active_right wrapper_unactive');
			$('header').toggleClass('wrapper_active_right wrapper_unactive');
			$('.dark_layer').on('click', function(){
				menu_icon_animator.animate_menu_icon($line1,$line2,$line3);
				$level2.removeClass('open2level').addClass('close2level');
				$('.wrapper').toggleClass('y_scrollable y_not_scrollable');
			});
		}

		function toggle_cart(){
			$('.wrapper').toggleClass('wrapper_active_left wrapper_unactive');
			$('#right_panel').toggleClass('right_panel_active right_panel_unactive');
			$('.dark_layer').toggleClass('layer_active layer_unactive');
			$('body').toggleClass('scrollable not_scrollable');
			$('header').toggleClass('wrapper_active_left wrapper_unactive');
			$('.dark_layer').toggleClass('wrapper_active_left wrapper_unactive');
			$('.toggle_menu').toggleClass('left_panel_toggler_unactive left_panel_toggler_active');
		}

		//menu icon -------------------------------------------------------
		$('body').on('click', '.toggle_menu',function(){  //event delegation
			if($level2.hasClass('open2level')){
				$level2.removeClass('open2level').addClass('close2level');
			}
			menu_icon_animator.animate_menu_icon($line1,$line2,$line3);
			$('.wrapper').toggleClass('y_scrollable y_not_scrollable');
		});

		//slide menu ------------------------------------------------------
		$('#left_panel').on('mouseenter', 'li', function(){  //event delegator
			var ID = $(this).attr('id');
			$level2.removeClass('open2level').addClass('close2level');
			$('.'+ID).toggleClass('open2level close2level');
		});

		$('.goback').on('click', function(){
			$level2.removeClass('open2level').addClass('close2level');
		});

		$('#left_panel li').hover(function(){
			$(this).addClass('li_hover');
		},function(){
			$(this).removeClass('li_hover');
		});

		$('.level2 li').hover(function(){
			$(this).addClass('li_hover');
		},function(){
			$(this).removeClass('li_hover');
		});

		//addToCart mustache injection -----------------------------------------------------
		$('.spare-price-buy').on('click', '.buy', function(){  // event delegation
			injecter.start($cart_counter_container,$panel_content);
			console.log('a');
		});

		// slick-carousel --------------------------------------
		$('.product_window .slider-container').slick({
			slide: '.slider-item',
			arrows: true,
			dots: true,
			autoplay: false,
			autoplaySpeed: 2000,
			prevArrow: '<span class="slick-prevv"><</span>',
			nextArrow: '<span class="slick-nextt">></span>',
			appendDots: '.product_window .slider-container .slider-nav',
			appendArrows: '.product_window .slider-container .slider-nav'
		});

		$('.carousel_suggestions .slider-container').slick({
			slide: '.slider-item',
			arrows: true,
			dots: true,
			prevArrow: '<span class="slick-prevv"><</span>',
			nextArrow: '<span class="slick-nextt">></span>',
			appendDots: '.carousel_suggestions .slider-container .slider-nav',
			appendArrows: '.carousel_suggestions .slider-container .slider-nav',
			responsive: [
				{
			      breakpoint: 3000,
			      settings: {
			        slidesToShow: 7,
			        slidesToScroll: 7
			      }
			    },
				{
			      breakpoint: 2300,
			      settings: {
			        slidesToShow: 6,
			        slidesToScroll: 6
			      }
			    },
				{
			      breakpoint: 1750,
			      settings: {
			        slidesToShow: 5,
			        slidesToScroll: 5
			      }
			    },
			    {
			      breakpoint: 1024,
			      settings: {
			        slidesToShow: 4,
			        slidesToScroll: 4
			      }
			    },
			    {
			      breakpoint: 760,
			      settings: {
			        slidesToShow: 3,
			        slidesToScroll: 3
			      }
			    },
			    {
			      breakpoint: 560,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
			      }
			    },
			    {
			      breakpoint: 400,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }]
		});

		$('.carousel_suggestions .slider-item').hover(
		function(){
			console.log('a');
			$(this).find('.hidden_buy').addClass('liftup_buy');
			$(this).find('.hidden_know_more').addClass('liftup_know_more');
		},
		function(){
			$(this).find('.hidden_buy').removeClass('liftup_buy');
			$(this).find('.hidden_know_more').removeClass('liftup_know_more');
		});

});