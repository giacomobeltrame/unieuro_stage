define(function(){

	function animate_menu_icon($line1,$line2,$line3){
		$line1.toggleClass('anim_open1 anim_close1');
		$line2.toggleClass('anim_open2 anim_close2');
		$line3.toggleClass('anim_open3 anim_close3');
		return true;
	}

	return{
		animate_menu_icon: animate_menu_icon
	};
});