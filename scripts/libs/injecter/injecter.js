define(["mustache"],function(mustache){

	function start($target1,$target2){
		return $.getJSON('data/cart.json',function(JSONData) {
	        $.get('templates/templates.html', function(template) {
	        	$template = $(template);
	            $target1.html(mustache.render($template.filter('#'+$target1[0].id).html(), JSONData));
	            $target2.html(mustache.render($template.filter('#'+$target2[0].id).html(), JSONData));
	        });
    	});

	}

	return{
		start: start
	};

});